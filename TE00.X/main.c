/** @file       main.c
    * @author   JFG
    * @version  V1.0.0
    * @date     13-04-2020
    * @brief    State machines 2

    \ main page Description
 *       
 */

#define _SUPPRESS_PLIB_WARNING
#define SYS_FREQ            (96000000UL)
#define PBCLK_FREQUENCY        (96 * 1000 * 1000)

#define CORE_TICK_RATE      (SYS_FREQ/2/1000) //1mS
#include "Inc/main.h"
#include "Inc/pic32_uart.h"

#define FW_VER "1.1"
#define HW_VER "1.1"
#define AUTHOR "JFG"

void main(void) {
    printf("Booting ...\n FW Version = %s, HW Version = %s.\n Author = %s.\n//",
        FW_VER,HW_VER,AUTHOR );
    while(1){

    }
}
